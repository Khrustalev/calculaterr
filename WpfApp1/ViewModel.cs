﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.ComponentModel;
using System.Collections.ObjectModel;
using Newtonsoft.Json;
using System.IO;

namespace WpfApp1
{
    
    public class ViewModel : INotifyPropertyChanged
    {

        public ObservableCollection<string> Collection { get; }

        public void Save()
        {
             var save = JsonConvert.SerializeObject(Collection);
             File.WriteAllText("memory.json", save);
        }

      

       

        public void Load()
        {
            if (File.Exists("memory.json") == false) return;
            var save= File.ReadAllText("memory.json");
            var memory = JsonConvert.DeserializeObject<string[]>(save);
            foreach (var item in memory)
            {
                Collection.Add(item);
            }
        }
        



        private double[] a = new double[256];
        private char[] exp;
        public int i = 0;
        
        




        public ViewModel()
        {
            _buttonCommand = new RelayCommand<string>(X => Result += X);
            Collection = new ObservableCollection<string>();
            Collection.CollectionChanged += Collection_CollectionChanged;
            Load();

           // _loginCommand = new RelayCommand<string>
        }

        private void Collection_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            Save();
            
        }

        public double Parse(string str)
        {
            exp = str.ToCharArray();
            double res = 0;

            res = Proc_E();
            str += "=";
            str += res;
            Collection.Add(str);

            return res;
            
        }

        private double Proc_M()
        {
            double x = 0;

            if (exp[i] == '(')
            {
                i++;
                x = Proc_E();
                if (exp[i] != ')')
                    return Double.NaN; 
                i++;
            }
            else if (exp[i] == '-')
            {
                i++;
                x = -Proc_M();
            }
            else if (char.IsDigit(exp[i]))
                x = Number();
            return x;
        }

        private double Proc_T()
        {
            double x = Proc_M();
            while(i < exp.Length && (exp[i] == '*' || exp[i] == '/'))
            {
                char p = exp[i];
                i++;
                if (p == '*')
                    x *= Proc_M();
                else
                    x /= Proc_M();
            }
            return x;
        }

        private double Proc_E()
        {
            double x = Proc_T();
            while ( i < exp.Length && (exp[i] == '+' || exp[i] == '-'))
            {
                char p = exp[i];
                i++;
                if (p == '+')
                    x += Proc_T();
                else
                    x -= Proc_T();
            }
            
            return x;
        }

        public double Number()
        {
            string number = string.Empty;
            while((i < exp.Length) && (char.IsDigit(exp[i]) || exp[i] == ','))
            {
                number += exp[i];
                i++;
            }
            
            return double.Parse(number);
        }

        public double Number(char[] exp)
        {
            string number = string.Empty;
            while((i < exp.Length) && (char.IsDigit(exp[i]) || exp[i] == ','))
            {
                number += exp[i];
                i++;
            }

            if(i == exp.Length)
            {
                i = 0;
                return 1;
            }

            i = 0;
            return 0;
        }

        public string _result = "0";

        public string Result
        {
            get {
                
                return _result;
                }

            set {
                
                _result = value;

                if(value!= "0")// для того что бы убрать 0
                {
                    if (_result[0] == '0') _result = _result[1].ToString();
                }
                
                OnPropertyChanged(nameof(Result));
                
                
                }
        }

        private void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
        private ICommand _clearCommand = new ClearCommand();
        public ICommand ClerCommand { get { return _clearCommand; } }

        private ICommand _buttonCommand = new ButtomCommand();
        public ICommand ButtonCommand { get { return _buttonCommand; } }

        private ICommand _equallCommand = new EquallyCommand();
        
        public ICommand EquallCommand { get {  return _equallCommand; }   }

        private ICommand _loginCommand = new ButtomCommand();
        public ICommand LoginCommand  { get { return _loginCommand; } }



        public event PropertyChangedEventHandler PropertyChanged;

    }

 

    public class ButtomCommand : ICommand
    {
        public void Execute(object parameter)
        {
            
            var mainViewModel = parameter as ViewModel;
            mainViewModel.Result += parameter.ToString();
        }

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public event EventHandler CanExecuteChanged
        {
            add
            {
                CommandManager.RequerySuggested += value;
            }
            remove
            {
                CommandManager.RequerySuggested -= value;
            }
        }
    }


    public class EquallyCommand : ICommand
    {
        public void Execute(object parameter)
        {
            var mainViewModel = parameter as ViewModel;
            mainViewModel.Result = mainViewModel.Parse(mainViewModel.Result).ToString();
            mainViewModel.i = mainViewModel.Result.Length - 1;

            
        }

        public bool CanExecute(object parameter)
        {
            var mainViewModel = parameter as ViewModel;
            if (mainViewModel == null)
                return false;
            int i = mainViewModel.Result.Length - 1;
            var charResult = mainViewModel.Result.ToCharArray();
            if (charResult[i] == '-' || charResult[i] == '+' || charResult[i] == '*' || charResult[i] == '/' || charResult[i] == '(' || mainViewModel.Number(mainViewModel.Result.ToCharArray()) == 1)
                return false;
            return true;
        }

        public event EventHandler CanExecuteChanged
        {
            add
            {
                CommandManager.RequerySuggested += value;
            }
            remove
            {
                CommandManager.RequerySuggested -= value;
            }
        }
    }

    public class ClearCommand : ICommand
    {
        public void Execute(object parameter)
        {
            var mainViewModel = parameter as ViewModel;
            mainViewModel.Result = "00";// для того что бы убрать 0 было(mainViewModel.Result = "0";)
            mainViewModel.i = 0;
        }

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public event EventHandler CanExecuteChanged
        {
            add
            {
                CommandManager.RequerySuggested += value;
            }
            remove
            {
                CommandManager.RequerySuggested -= value;
            }
        }
    }


    

    public class RelayCommand : ICommand
    {
        private readonly Action _execute;
        private readonly Func<bool> _canExecute;

        public RelayCommand(Action execute, Func<bool> canExecute = null)
        {
            _execute = execute ?? throw new ArgumentNullException(nameof(execute));
            _canExecute = canExecute;
        }

        public void Execute(object parameter)
        {
                _execute.Invoke();
        }

        public bool CanExecute(object parameter)
        {
                return _canExecute?.Invoke() ?? true;
        }

        public event EventHandler CanExecuteChanged
        {
            add
            {
                CommandManager.RequerySuggested += value;
            }
            remove
            {
                CommandManager.RequerySuggested -= value;
            }
        }
    }

    public class RelayCommand<T> : ICommand
    {
        private readonly Action<T> _execute;
        private readonly Func<T, bool> _canExecute;

        public RelayCommand(Action<T> execute, Func<T, bool> canExecute = null)
        {
            _execute = execute ?? throw new ArgumentNullException(nameof(execute));
            _canExecute = canExecute;
        }

        public void Execute(object parameter)
        {
            if(parameter is T arg)
            {
                _execute.Invoke(arg);
            }
        }

        public bool CanExecute(object parameter)
        {
            if(parameter is T arg)
            {
                return _canExecute?.Invoke(arg) ?? true;
            }
            return false;
        }

        public event EventHandler CanExecuteChanged
        {
            add
            {
                CommandManager.RequerySuggested += value;
            }
            remove
            {
                CommandManager.RequerySuggested -= value;
            }
        }
    }
}


